// Теоретичні питання

// 1. Опишіть своїми словами, що таке метод об'єкту.
// 2. Який тип даних може мати значення властивості об'єкта?
// 3. Об'єкт це посилальний тип даних. Що означає це поняття?

// 1. Це функція в об"єкті, яка використовується для роботи з властивостями об"єкта в області видимості самого об"єкта.
// 2. Будь-який тип данних може бути властивістю об"єкта.
// 3. При копіюванні об"єкта він копіюється у вигляді посилання на оригінальний об"єкт із посиланнями на властивості і значення.

let firstName = prompt("Ente your name").slice(0, 1);
let lastName = prompt("Ente your last name");
function createNewUser(firstName, lastName) {
  return {
    firstName,
    lastName,

    getLogin() {
      console.log(`${this.firstName}${this.lastName}`.toLowerCase());
    },
  };
}
const newUser = createNewUser(firstName, lastName);
newUser.getLogin();
